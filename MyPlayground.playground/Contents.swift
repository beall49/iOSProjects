//: Playground - noun: a place where people can play

import UIKit

func dashatize(_ number: Int) -> String {
    var returnString: [String] = []
    var prev = 0
    let _number = (number < 0) ? (number * -1) : number
    
    for n in String(_number).characters {
        let num = Int(String(n))
        let syze = returnString.count
        var cur = 0
        
        if num! % 2 != 0 {
            cur = 1
        }
        
        var appendString = ""
        if syze > 0 {
            if prev == 1 || cur == 1 {
                appendString = "-"
            }
        }
        prev = cur
        returnString.append ("\(appendString)\(num!)" )
    }
    return returnString.joined(separator: "")
}

//print(dashatize(-28369))


func dontGiveMeFive(_ start: Int, _ end: Int) -> Int {
    var cnt = 0
    for n in start...end {
        if !(String(n).contains("5")){
            cnt+=1
        }
    }
    return cnt
}


//print (dontGiveMeFive(4,17))



func accum(_ s: String) -> String {
    var returnList: [String] = []
    
    for (index, ele)  in s.uppercased().characters.enumerated() {
        let element = String(ele)
        let seperator = (index>0 ? "-" : "")
        var currentString = element
        for _ in 0..<index{
            currentString += element.lowercased()
        }
        returnList.append("\(seperator)\(currentString)")
    }
    
    return returnList.joined()
}


//print (accum("RqaEzty"))


let arr = [74,75,78,82,89,66,41]

print(arr)








