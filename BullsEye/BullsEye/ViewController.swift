import UIKit
import QuartzCore
class ViewController: UIViewController {
    let defaultSliderValue: Float = 50
    
	@IBOutlet weak var lblTarget: UILabel!
	@IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var lblRound: UILabel!
    @IBOutlet weak var btnStartOver: UIButton!
    @IBOutlet weak var slider: UISlider! {
        didSet {
            slider.value = defaultSliderValue
            slider.setThumbImage(UIImage(named: "SliderThumb-Normal"), forState: .Normal)
            slider.setThumbImage(UIImage(named: "SliderThumb-Highlighted"), forState: .Highlighted)

            let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)

            if let trackLeftImage = UIImage(named: "SliderTrackLeft") {
                let trackLeftResizable = trackLeftImage.resizableImageWithCapInsets(insets)
                slider.setMinimumTrackImage(trackLeftResizable, forState: .Normal)
            }
            
            if let trackRightImage = UIImage(named: "SliderTrackRight") {
                let trackRightResizable = trackRightImage.resizableImageWithCapInsets(insets)
                slider.setMaximumTrackImage(trackRightResizable, forState: .Normal)
            }
        }
    }   
    
	var labels: [(lbl: UILabel, txt: String)] = []
	var targetValue: Int = Int(arc4random_uniform(100))
	var strMessage: String = "The value of the slider is :%u \nThe target value is :%u \nYour overall score is: %u"
	var score: Int = 0
	var currentValue: Int = 0
	var round: Int = 1

	override func viewDidLoad() {
		super.viewDidLoad()
		startNewRound()
	}

  
    func startOver(){

        slider.value = defaultSliderValue
        score = 0
        round = 0
        let transition = CATransition()
        transition.type = kCATransitionFade
        transition.duration = 1
        transition.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseOut)
        view.layer.addAnimation(transition, forKey: nil)
        startNewRound()
    }
    
	func startNewRound() {
		targetValue = 1 + Int(arc4random_uniform(100))
		labels.append((lbl: lblTarget, txt: String(targetValue)))
		labels.append((lbl: lblScore, txt: String(score)))
		labels.append((lbl: lblRound, txt: String(round)))
		updateLabels()
		round += 1
	}

    func updateLabels() {
		for element in labels {
			element.lbl.text = element.txt
		}
		labels.removeAll()
	}

    func getTitle(points: Int) -> String {
        switch (points) {
            case 0:
                return "Perfect"
            case _ where points < 5:
                return "Super Close"
            case _ where points < 10:
                return "Pretty Good"
            default:
                return "Why are you even playing?"
        }
	}
    
    @IBAction func resetValues(){
        startOver()
    }
    
	@IBAction func showAlert() {
		let points = abs(currentValue-targetValue)

		score += points

		let message = String(format: strMessage, arguments: [currentValue, targetValue, score])

		let alert = UIAlertController(title: getTitle(points), message: message, preferredStyle: .Alert)

        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: {action in self.startNewRound()}))

		presentViewController(alert, animated: true, completion: nil)
	}

	@IBAction func sliderMoved(slider: UISlider) {
		currentValue = lroundf(slider.value)
	}
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

