//
//  AboutViewController.swift
//  BullsEye
//
//  Created by Beall, Ryan on 8/4/16.
//  Copyright © 2016 com.beall.ryan. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!{
        didSet{
            if let htmlFile = NSBundle.mainBundle().pathForResource("BullsEye", ofType: "html") {
                if let htmlData = NSData(contentsOfFile: htmlFile) {
                    let baseURL = NSURL(fileURLWithPath: NSBundle.mainBundle().bundlePath)
                    webView.loadData(htmlData, MIMEType: "text/html", textEncodingName: "UTF-8", baseURL: baseURL)
                }
            }
        }
    }
    
    @IBAction func close(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    

}
