import UIKit
import RxSwift
import Alamofire
import AlamofireImage
import RxAlamofire
import RealmSwift

class WorkoutTableViewController: UITableViewController {
	// properties
	var workouts = Results<TBL_WORKOUT>?()
	let tbl = TBL_WORKOUT()
	let cellIdentifier = "WorkoutTableViewCell"
	let entries: String = "http://rbeall49.pythonanywhere.com/api/entries/15"
	var bag: DisposeBag! = DisposeBag()

	var realm = try! Realm()

	override func viewDidLoad() {
		super.viewDidLoad()
		// print(Realm.Configuration.defaultConfiguration.fileURL!)
		if tbl.getResultCount() == 0 {
			self.checkForData()
		}
		self.refreshControl?.addTarget(self,
			action: #selector(handleRefresh(_:)),
			forControlEvents: UIControlEvents.ValueChanged)
	}

	// set workout data into cell
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let workout = TBL_WORKOUT().getSingleRow(indexPath.row)
		let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! WorkoutTableViewCell
		cell.lblTitle.text = workout.getDayOfWeek()
		cell.lblPostText.text = workout.getCleanTitle()
		cell.lblId.text = workout.id
		cell.photoImage.af_setImageWithURL(workout.getDownloadURL())
		return cell
	}

	// get workout data to give to single view
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		let WorkoutDetailViewController = segue.destinationViewController as! WorkoutViewController

		if let selectedCell = sender as? WorkoutTableViewCell {
			let indexPath = tableView.indexPathForCell(selectedCell)!
			let selectedWorkout = TBL_WORKOUT().getSingleRow(indexPath.row)
			WorkoutDetailViewController.workout = selectedWorkout
		}
	}

	// check web server for data
	func checkForData() -> Void {
		RxAlamofire.requestJSON(Method.GET, entries)
			.observeOn(MainScheduler.instance)
			.subscribe(onNext: { (r, json) in
				if let arr = json as? [[String: AnyObject]] {
					try! self.realm.write({
						for row in arr {
							self.realm.create(TBL_WORKOUT.self, value: row, update: true)
						}
					})
				}
            },
            onCompleted: {
                self.tableRefresh()
            }).addDisposableTo(bag)
	}

	func tableRefresh() -> Void {
		self.tableView.reloadData()
		self.refreshControl?.endRefreshing()
	}

	// on refresh
	func handleRefresh(refreshControl: UIRefreshControl) { self.checkForData() }

	// overrides
	override func didReceiveMemoryWarning() { super.didReceiveMemoryWarning() }

	override func numberOfSectionsInTableView(tableView: UITableView) -> Int { return 1 }

	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return tbl.getResultCount() }

}
