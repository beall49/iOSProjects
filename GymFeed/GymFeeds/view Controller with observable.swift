//
//  ViewController.swift
//  GymFeeds
//
//  Created by Beall, Ryan on 8/8/16.
//  Copyright © 2016 com.beall.ryan. All rights reserved.
//

import UIKit
import RxSwift
import SwiftyJSON
import ReactiveKit
import ReactiveUIKit

class ViewController: UIViewController {
	@IBOutlet weak var btnText: UIButton!
	@IBOutlet weak var lblView: UITextView!
    
	var get = ObservableInterface()
    
	var token = Property<String?>("Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.")

	override func viewDidLoad() {
		super.viewDidLoad()
		token.bindTo(lblView)
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}

    
	@IBAction func getBtnAction() {
        get.getBasicAPI() { data , error in
            self.token.value = (data?.objectAtIndex(0).valueForKey("name") as? String)
        }

        
        get.getBasicAPI() { data , error in
            let jsonArr: JSON = JSON(data!)

            var nameText: String = ""
            var names: String = ""
            var years: String = ""
            
            for (_, vals): (String, JSON) in jsonArr {
                names = vals["name"].string!
                years = vals["year"].string!
                
                nameText = nameText + names + ":" + years + "\n"
            }
            self.token.value = nameText
            //(data?.objectAtIndex(0).valueForKey("name") as? String)
        }
        
        
    }
    
    func voider(){
        get.getObservableAPI()
            //.subscribeOn(ConcurrentDispatchQueueScheduler(globalConcurrentQueueQOS: .Background))
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: {
                    data in
                    do {
                        //try NSJSONSerialization.JSONObjectWithData(data as! NSData, options: []) as! NSDictionary).valueForKey("token") as? String
                        self.token.value =  (
                            try NSJSONSerialization.JSONObjectWithData(data as! NSData, options: []) as! NSArray)
                            .objectAtIndex(0)
                            .valueForKey("name") as? String
                        
                    } catch {
                        return
                    }
                },
                onError: { error in print(error) },
                onCompleted: { print("") },
                onDisposed:{ print("disposed") }
        )
    }

}