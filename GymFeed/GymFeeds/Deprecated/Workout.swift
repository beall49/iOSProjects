//
//  File.swift
//  GymFeeds
//
//  Created by Beall, Ryan on 8/30/16.
//  Copyright © 2016 com.beall.ryan. All rights reserved.
//
import UIKit
import Foundation

class Workout: NSObject, NSCoding {
	var title: String
	var post: String
	var url: String
	var id: String
	var image: String
	var clean_date: String
    var daysOfTheWeek = NSDateFormatter().weekdaySymbols
    
	static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
	static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("workouts")

	init?(title: String, post: String, url: String, current_id: String, image: String?, clean_date: String) {
		self.title = title
		self.post = post
		self.url = url
		self.id = current_id
		self.image = (image)!
		self.clean_date = clean_date

		super.init()

        if self.image.isEmpty || url.isEmpty {
			return nil
		}
	}
    
    func getPostText() -> String {
        return self.post.stringByReplacingOccurrencesOfString("\n", withString: "\n\n")
    }
    
    func getDefaultImage() -> UIImage {
        return UIImage(named:  "defaultPhoto")!
    }
    
    func getDownloadURL() -> NSURL{
        return NSURL(string: self.image)!
    }
    
    func getDayOfWeek ()-> String {
        for dayOf in daysOfTheWeek {
            if self.title.rangeOfString(dayOf) != nil {
                return dayOf
            }
        }
        return daysOfTheWeek[0]
    }
    
    func getCleanTitle () -> String{
        for dayOf in daysOfTheWeek {
            if self.title.rangeOfString(dayOf) != nil {
                return self.title.stringByReplacingOccurrencesOfString(dayOf, withString: "")
                                .stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            }
        }
        return self.title
    }

    //put into array
	struct PropertyKey {
		static let titleKey = "title"
		static let postKey = "post"
		static let urlKey = "url"
		static let idKey = "id"
		static let imageKey = "image"
		static let cleanDateKey = "clean_date"
	}

    //for each key in propkeys
	func encodeWithCoder(aCoder: NSCoder) {
		aCoder.encodeObject(title, forKey: PropertyKey.titleKey)
		aCoder.encodeObject(post, forKey: PropertyKey.postKey)
		aCoder.encodeObject(url, forKey: PropertyKey.urlKey)
		aCoder.encodeObject(id, forKey: PropertyKey.idKey)
		aCoder.encodeObject(image, forKey: PropertyKey.imageKey)
		aCoder.encodeObject(clean_date, forKey: PropertyKey.cleanDateKey)
	}

	required convenience init?(coder aDecoder: NSCoder) {
		let title = aDecoder.decodeObjectForKey(PropertyKey.titleKey) as! String
		let post = aDecoder.decodeObjectForKey(PropertyKey.postKey) as! String
		let url = aDecoder.decodeObjectForKey(PropertyKey.urlKey) as! String
		let id = aDecoder.decodeObjectForKey(PropertyKey.idKey) as! String

		let image = aDecoder.decodeObjectForKey(PropertyKey.imageKey) as! String
		let clean_date = aDecoder.decodeObjectForKey(PropertyKey.cleanDateKey) as! String

		self.init(title: title, post: post, url: url, current_id: id, image: image, clean_date: clean_date)
	}

	static func createWorkoutsFromArray(json: Array<[String: AnyObject]>) -> Array<Workout> {
		var workouts = [Workout]()

		var title: String
		var post_text: String
		var id: String
		var url: String
		var image: String
		var clean_date: String

		for row in json {
			title = row["title"] as! String
			post_text = row["post_text"] as! String
			image = (row["image"] as! String)
			id = (row["id"] as! String)
			url = (row["url"] as! String)
			clean_date = (row["clean_date"] as! String)

			let temp = Workout(title: title, post: post_text, url: url, current_id: id, image: image, clean_date: clean_date)!
			workouts.append(temp)
		}

		return workouts
	}

}