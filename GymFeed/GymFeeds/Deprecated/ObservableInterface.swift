//
//  GeoToken.swift
//  GymFeeds
//
//  Created by Beall, Ryan on 8/8/16.
//  Copyright © 2016 com.beall.ryan. All rights reserved.
//
import Alamofire
import RxAlamofire
import RxSwift
import RxCocoa
import SwiftyJSON
import Foundation
import ReactiveKit

class ObservableInterface {
    let earl: String = "http://rbeall49.pythonanywhere.com/api/entries/15"


    func getDatas() -> Void {
        RxAlamofire.requestJSON(Method.GET, self.earl)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { (r,json) in
                if let arr = json as?  Array<[String: AnyObject]> {
                    print (arr)
                    //Workout.createWorkoutsFromArray(arr)
                }

            }).addDisposableTo(DisposeBag())
    }



    func getObservableAPI() -> Observable<Any?> {
        return Observable.create {
            observer in let request = Alamofire.request(.GET, self.earl)
                .response(completionHandler: { request, response, data, error in

                    if ((error) != nil) {
                        observer.on(.Error(error!))
                    } else {
                        observer.on(.Next(data ?? NSData()))
                        observer.on(.Completed)
                    }
                });
            return AnonymousDisposable {
                request.cancel()
            }
        }
    }

    //easy to take apart jsons
    func getBasicAPI(completionHandler: (NSArray?, NSError?) -> ()) {
        Alamofire.request(.GET, earl, parameters: nil)
            .responseJSON { response in
                switch response.result {
                case .Success(let value):

                    completionHandler(value as? NSArray, nil)
                case .Failure(let error):
                    completionHandler(nil, error)
                }
        }
    }

    // check web server for data
    func checkForData1() -> Void {
        Alamofire.request(logURL).responseJSON { response in
//            print(response.request)  // original URL request
//            print(response.response) // HTTP URL response
//            print(response.data)     // server data
//            print(response.result)   // result of response serialization

            if let JSON = response.result.value as? Dictionary<String, AnyObject> {
                print ( Mirror(reflecting: JSON))
                print("JSON: \(JSON["resultSet1"])")
            }
        }
    }    



}
