import UIKit
import RxSwift
import SwiftyJSON
import ReactiveKit
import ReactiveUIKit

class WorkoutViewController: UIViewController, UINavigationControllerDelegate {
    var workout : TBL_WORKOUT?
    let daysOfWeek = NSDateFormatter().weekdaySymbols

    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var photoImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let workout = workout  {
            navigationItem.title = workout.getCleanTitle()
            txtView.text   = workout.getPostText()            
            photoImage.af_setImageWithURL(workout.getDownloadURL())
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }   
}