import Foundation
import RealmSwift

class TBL_WORKOUT: Object {
	dynamic var title: String = ""
	dynamic var post_text: String = ""
	dynamic var url: String = ""
	dynamic var current_id: String = ""
	dynamic var image: String = "http://timecard.ryanbeall.com/images/Ryan.png"
	dynamic var clean_date: String = ""
    dynamic var id: String = ""
    dynamic var date_add: String = "" 
	let daysOfTheWeek = NSDateFormatter().weekdaySymbols
    
    override static func primaryKey() -> String? {
        return "current_id"
    }
    
    func getResults() -> Results<TBL_WORKOUT>? {
        let localrealm = try! Realm()
        let results = localrealm.objects(TBL_WORKOUT.self)
        return results.count > 0 ? results : nil
    }
    
    func getResultCount() -> Int {
        let localrealm = try! Realm()
        let results = localrealm.objects(TBL_WORKOUT.self)
        return results.count
    }
    
    func getPostText() -> String {
       return self.post_text.stringByReplacingOccurrencesOfString("\n", withString: "\n\n")
    }
    
    func getDownloadURL() -> NSURL{
        return NSURL(string: "http://timecard.ryanbeall.com/images/Ryan.png")!
    }
    
    func getDayOfWeek ()-> String {
        for dayOf in daysOfTheWeek {
            if self.title.rangeOfString(dayOf) != nil {
                return dayOf
            }
        }
        return daysOfTheWeek[0]
    }
    
    func getCleanTitle ( ) -> String{
        for dayOf in daysOfTheWeek {
            if self.title.rangeOfString(dayOf) != nil {
                return self.title
                    .stringByReplacingOccurrencesOfString(dayOf, withString: "")
                    .stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            }
        }
        return ""
    }
    
    func getSingleRow(index: Int) -> TBL_WORKOUT {
        return try! Realm().objects(TBL_WORKOUT.self).sorted("current_id", ascending: false)[index]
    }

}
