import RealmSwift
class TempLog: Object {
    dynamic var _id: Int = 0
    dynamic var tran_date: String?
    dynamic var tran_type: String?
    dynamic var path: String?
    dynamic var error_message: String?
    dynamic var in_payload_type: String?
    dynamic var incoming: String?
    dynamic var out_payload_type: String?
    dynamic var outgoing: String?
    
    public static func deleteAllOfMe(){
        let this = try! Realm()        
        if let _ = this.objects(TempLog.self).first {
            try! this.write {
                this.deleteAll()
            }
        }
    }
}
