import UIKit
class TableViewCell: UITableViewCell {

    @IBOutlet weak var lblSummary: UILabel!
    @IBOutlet weak var lblError: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
