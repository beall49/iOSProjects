//
//  Requests.swift
//  LogViewer
//
//  Created by Beall, Ryan on 10/4/16.
//  Copyright © 2016 com.g3.enterprises. All rights reserved.
//



import RxSwift
import RealmSwift
import Alamofire
import RxAlamofire


class Requests {
    var log_count = false;
    var realm = try! Realm()
    let disposeBag: DisposeBag = DisposeBag()
    let logHost = "https://g3enterprises.cloudhub.io"
    var logObject: [String: [AnyObject]]?
    
    func getData(type: String, minutes: String, status: String)  -> Void {
        let logURL = "\(logHost)/api/logs?type=\(type)&minutes=\(minutes)&status=\(status)"
        
        RxAlamofire.requestJSON(.get, logURL )
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { (r, json) in
                    if let arr = json as? [String: [AnyObject]] {
                        self.logObject = arr
                    }
                },
                onError: { error in
                    print("my ERRORRRRRRRR \(error)")
                },
                onCompleted: {
                    guard let object = self.logObject?["resultSet1"] else {
                        return
                    }
                    try! self.realm.write({
                        for row in object  {
                            self.realm.create(TempLogs.self, value: row)
                        }
                    })
                    self.log_count = true
                }
            ).addDisposableTo(disposeBag)
    }
    
    func setLogCount(){
        log_count = false
    }
    
}


