//print(Realm.Configuration.defaultConfiguration.fileURL!)

import Foundation
import UIKit
#if !RX_NO_MODULE
    import RxSwift
    import RxCocoa
#endif
import RealmSwift
import RxRealm

class MainViewController: UIViewController,  UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    let request = Request()
    let disposeBag = DisposeBag()
    var realm = try! Realm()
    let cellIdentifier = "LogTableCell"
    let end_points = EndPointNames.self
    var minutes = Variable<String>("")
    var errorsOnly = Variable<String>("2")
    var endPointName = Variable<String>("ALL")
    var errorFilter = Variable<String>("error_message = error_message")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addObservers()
        setUpNavBar()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true);
        return true;
    }
    
    func errHandler(){
        self.progressSpinner.stopAnimating()
    }
    
    func getLogs() -> Results<TempLog> {
        return realm
            .objects(TempLog.self)
            .filter(errorFilter.value)
            .sorted(byProperty:  "tran_date", ascending: false)
        
    }
    
    /*
     replaced .addDisposableTo(disposeBag) with _ =
     */
    func addObservers() {
        
        var logs = getLogs()
        let tableData = Observable.from(logs).shareReplay(0)
        
        txtMinutes
            .rx
            .text
            .orEmpty
            .bindTo(minutes)
            .addDisposableTo(disposeBag)
        
        
        //bind data source (observers to table)
        _ = tableLogs
            .rx
            .setDelegate(self)
        
        //bind switch to errors only variable and
        _ = switchError.rx.value
            .subscribe(
                onNext: { x in
                    self.errorFilter.value = ("error_message \(x ? "!= 'No error'" : "= error_message") ")


                    self.lblError.isEnabled = x
                    self.errorsOnly.value = "\(2 + ((x) ? 1 : 0))"
                },
                onCompleted: {
                    logs = self.getLogs()
                    self.tableLogs.reloadData()
                }
        
        )

        
        
        //bind picker to name variable
        _ = pickerEndPoints.rx
            .itemSelected.map({
                (EndPointNames(rawValue: $0.0)?.name())!
            })
            .bindTo(endPointName)
        
        
        //button click
        _ = btnSubmit.rx.tap
            .subscribe(
                onNext: { x in
                    self.view.endEditing(true);
                    self.progressSpinner.startAnimating()
                    TempLog.deleteAllOfMe()
                    self.request
                        //need to create object here
                        .getData(
                            name: self.endPointName.value,
                            min: self.minutes.value,
                            err: self.errorsOnly.value,
                            context: self
                    )
            })
        
        
        //bind records returned label to data
        _ = tableData.map({
            return "\($0.count) Records Returned "
        }).bindTo(self.lblRecordsReturned.rx.text)
        
        //update table cells whenever the data changes
        _ = tableData
            .bindTo(
                tableLogs.rx.items(
                    cellIdentifier: cellIdentifier, cellType: TableViewCell.self)) {(row, element, cell) in
                        let err = (element.error_message?.uppercased() != "NO ERROR") ? "❌" : "✅"
                        cell.lblSummary!.text = " \(err) \(element.tran_date!)\n \(EndPointNames.getValueByName(incoming: element.tran_type!))"
        }
    }
    
    //handle seque
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let LogDetailViewController = segue.destination as! LogDetailViewController
        
        if let selectedCell = sender as? TableViewCell {
            let indexPath = tableLogs.indexPath(for: selectedCell)!
            let selectedWorkout = realm.objects(TempLog.self).sorted(byProperty:  "tran_date", ascending: false)[indexPath.row]
            LogDetailViewController.log = selectedWorkout
        }
    }
    
    func setUpNavBar(){
        //dostuff
        let nav  = UINavigationBar()
        nav.layer.shadowColor = UIColor.black.cgColor
        nav.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        nav.layer.shadowOpacity = 0.24
        nav.layer.shadowRadius = CGFloat(16.0)
    }
    
    ///////////////////////////////⚠️boiler plate and constants⚠️
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { return 80 }
    
    func numberOfSections(in tableView: UITableView) -> Int { return 1 }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int { return 1 }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return realm.objects(TempLog.self).count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return EndPointNames.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return  (EndPointNames(rawValue: row)?.text())!
    }
    
    func giveObjectABorder(object: AnyObject){
        object.layer.borderWidth = 1.3
    }
    
    
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var switchError: UISwitch!
    @IBOutlet weak var lblRecordsReturned: UILabel!{
        didSet {
            lblRecordsReturned.text = ""
        }
    }
    @IBOutlet weak var tableLogs: UITableView!{
        didSet {
            giveObjectABorder(object: tableLogs)
        }
    }
    
    @IBOutlet weak var pickerEndPoints: UIPickerView! {
        didSet {
            pickerEndPoints.delegate = self
            pickerEndPoints.dataSource = self
            giveObjectABorder(object: pickerEndPoints)
        }
    }
    
    @IBOutlet weak var txtMinutes: UITextField! {
        didSet {
            txtMinutes.text = "15"
        }
    }
    
    @IBOutlet weak var btnSubmit: UIButton! {
        didSet {
            btnSubmit.setImage(UIImage(named: "go"), for: .normal)
            giveObjectABorder(object: btnSubmit)
        }
    }
    @IBOutlet weak var progressSpinner: UIActivityIndicatorView! {
        didSet {
            progressSpinner.transform = CGAffineTransform(scaleX: 2.5, y: 2.5)
        }
    }
    
    public var stopSpinning: Bool = false {
        didSet(newValue){
            if (newValue) {
                stopSpinning = true
            } else {
                stopSpinning = false
                self.progressSpinner.stopAnimating()
            }
        }
    }
}
