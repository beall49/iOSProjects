enum EndPointNames: Int {
    case ALL = 0, AP_VOUCHER, AP_VOUCHER_UPDATE, AR_PAYMENT, AR_PAYMENT_UPDATE, EDI_INVOICE,
    GEO_EVENTS, TMS_SHIPMENT, TMS_SHIPMENT_UPDATES, UNPLANNED_ORDERS
    
    func name() -> String {
        return EndPointNames.endPointNames[self.rawValue].name
    }
    
    func text() -> String {
        return EndPointNames.endPointNames[self.rawValue].text
    }
    
    static func getValueByIndex(row: Int) -> (String, String) {
        return EndPointNames.endPointNames[row]
    }
    
    static func getValueByName(incoming: String) -> (String) {
        for endpoint in EndPointNames.endPointNames{
            if endpoint.name == incoming{
                return endpoint.text
            }
        }
        return "ALL"
    }
    
    static let count: Int = {
        var max: Int = 0
        while let _ = EndPointNames(rawValue: max) { max += 1 }
        return max
    }()
        
    static let  endPointNames = [
        (name: "ALL", text: "Logs"),
        (name: "AP_VOUCHER", text: "AP Voucher"),
        (name: "AP_VOUCHER_UPDATE", text: "AP Voucher Status"),
        (name: "AR_PAYMENT", text: "AR Payment"),
        (name: "AR_PAYMENT_UPDATE", text: "AR Payment Status"),
        (name: "EDI_INVOICE", text: "EDI Invoice"),
        (name: "TMS_SHIPMENT", text: "New Shipments"),
        (name: "TMS_SHIPMENT_UPDATE", text: "Shipment Updates"),
        (name: "TMS_SHIPMENT_SETTLEMENT", text: "Shipment Updates"),
        (name: "UNPLANNED_ORDERS", text: "Unplanned Orders")
    ]
}
