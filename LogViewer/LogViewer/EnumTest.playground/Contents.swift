//: Playground - noun: a place where people can play


struct EndPoint {
    let name: String
    let text: String
}




enum EndPointNames: Int {
    case ALL = 0, AP_VOUCHER, AP_VOUCHER_UPDATE, AR_PAYMENT, AR_PAYMENT_UPDATE, EDI_INVOICE,
                  GEO_EVENTS, TMS_SHIPMENT, TMS_SHIPMENT_UPDATES, UNPLANNED_ORDERS
    
    func name() -> String {
        return EndPointNames.endPointNames[self.rawValue].name
    }
    
    func text() -> String {
        return EndPointNames.endPointNames[self.rawValue].text
    }
    
    static let  endPointNames = [
        (name: "ALL", text: "ALL"),
        (name: "AP_VOUCHER", text: "AP Voucher"),
        (name: "AP_VOUCHER_UPDATE", text: "AP Voucher Status"),
        (name: "AR_PAYMENT", text: "AR Payment"),
        (name: "AR_PAYMENT_UPDATE", text: "AR Payment Status"),
        (name: "EDI_INVOICE", text: "EDI Invoice"),
        (name: "GEO_EVENTS", text: "GPS"),
        (name: "TMS_SHIPMENT", text: "New Shipments"),
        (name: "TMS_SHIPMENT_UPDATES", text: "Shipment Updates"),
        (name: "UNPLANNED_ORDERS", text: "Unplanned Orders")
    ]
}

EndPointNames.AP_VOUCHER.text()
//enum EndPointNames : EndPoint  {
//    case ALL(EndPoint)
    // EndPoint(name: "ALL",  text:"ALL")
//    case ALL1(EndPoint)
    //EndPoint(name: "ALL",  text:"ALL")
//    ,
//        (name: "AP_VOUCHER", text: "AP Voucher"),
//        (name: "AP_VOUCHER_UPDATE", text: "AP Voucher Status"),
//        (name: "AR_PAYMENT", text: "AR Payment"),
//        (name: "AR_PAYMENT_UPDATE", text: "AR Payment Status"),
//        (name: "EDI_INVOICE", text: "EDI Invoice"),
//        (name: "GEO_EVENTS", text: "GPS"),
//        (name: "TMS_SHIPMENT", text: "New Shipments"),
//        (name: "TMS_SHIPMENT_UPDATES", text: "Shipment Updates"),
//        (name: "UNPLANNED_ORDERS", text: "Unplanned Orders")
    
//}
