import UIKit

class LogDetailViewController: UIViewController {

    var log : TempLog! {
        didSet {
            navigationItem.title = EndPointNames.getValueByName(incoming: log.tran_type!)
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    @IBOutlet weak var btnError: UIBarButtonItem!
    @IBOutlet weak var txtOutgoing: UITextView!
    @IBOutlet weak var txtIncoming: UITextView!
    @IBOutlet weak var txtTime: UITextField!
    var errorText = ""
    
    @IBAction func errButtonPress(_ sender: AnyObject) {
        let errActionSheet = UIAlertController(title: "Error", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        // err action button
        let errorButton = UIAlertAction(title: errorText, style: UIAlertActionStyle.cancel) { (action) in print("Pressed Something")}
        errActionSheet.addAction(errorButton)
        present(errActionSheet, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        txtTime.text = log.tran_date
        txtIncoming.text = log.incoming
        txtOutgoing.text = log.outgoing
        errorText = log.error_message!
        if (errorText.uppercased() != "NO ERROR") {
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
