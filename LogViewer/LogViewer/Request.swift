import RxSwift
import RxAlamofire
import RealmSwift

class Request {
    let logHost = "https://g3enterprises.cloudhub.io"
    var realm = try! Realm()
    var logObject: [String: [AnyObject]]?
    let disposeBag = DisposeBag()
    
    func getData(name: String, min: String, err: String, context: MainViewController) -> Void {
        let logURL = "\(logHost)/api/logs/minutes-back?type=\(name)&minutes=\(min)&status=\(err)"
        print(logURL)
        RxAlamofire.requestJSON(.get, logURL )
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { (r, json) in
                    if let arr = json as? [String: [AnyObject]] {
                        self.logObject = arr
                    }
                },
                onError: { error in
                    print("my ERRORRRRRRRR \(error)\n\n\n\n\n")
                    self.stopUISpinner(context: context)
                },
                onCompleted: {
                    
                    guard let object = self.logObject?["resultSet1"] else {
                        //self.stopUISpinner(context: context)
                        return
                    }
                    
                    try! self.realm.write({
                        for row in object  {
                            self.realm.create(TempLog.self, value: row)
                        }
                    })
                },
                onDisposed: {
                    self.stopUISpinner(context: context)
                }
            ).addDisposableTo(disposeBag)
    }
    
    func stopUISpinner(context: MainViewController){
        context.stopSpinning = false
    }
}
