/*
      TimeEntries.swift
      TimeTracker

      Created by Beall, Ryan on 10/18/16.
      Copyright © 2016 com.g3.enterprises. All rights reserved.

    print(Realm.Configuration.defaultConfiguration.fileURL!)
     🍕
     Slice of pizza
     Unicode: U+1F355, UTF-8: F0 9F 8D 95
     ⏰
     Alarm clock
     Unicode: U+23F0, UTF-8: E2 8F B0
 
     🤔
     Thinking face
     Unicode: U+1F914, UTF-8: F0 9F A4 94
 */

import RealmSwift


class TimeEntries: Object {
    let dayTimePeriodFormatter = DateFormatter()
    
    dynamic var _id: String?
    dynamic var clock_date: String?
    dynamic var clock_out: String?
    dynamic var clock_in: String?
    dynamic var hours: String?
    dynamic var day_name: String?
    dynamic var lunch: String?
    
    public static func deleteAllOfMe(){
        let this = try! Realm()
        if let _ = this.objects(TimeEntries.self).first {
            try! this.write {
                this.deleteAll()
            }
        }
    }
    
    func getTodaysDate() -> String {
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: Date() )
    }
    
    func get_id() -> String{
        return self._id ?? "";
    }

    func get_date() -> String{
        return self.clock_date ?? getTodaysDate()
    }
    
    func get_in() -> String{
        return self.clock_in ?? "💩"
    }
    
    func get_out() -> String{
        return self.clock_out?.replacingOccurrences(of: " ", with: "") ?? "⏰"
    }
    
    func get_day() -> String{
        return self.day_name ?? "💩"
    }
    
    func get_hours() -> String{
        return self.hours ?? "🤔"
    }
    
    func get_lunch() -> String{
        return self.lunch ?? "🍕"
    }
    
    func getAlertEntries(index: Int) -> EntryRow {
        let entry = try! Realm().objects(TimeEntries.self).sorted(byKeyPath: "clock_date", ascending: false)[index]
        let _in = "In: \(entry.get_in())"
        let lunch = "Lunch: \(entry.get_lunch())"
        let out = "Out: \(entry.get_out()) "
        let total = "Total: \(entry.get_hours()) "
        var _date = entry.get_date()
        
        let components = TimeEntries.formatDate(incoming: _date)
        if let month = components?.month, let day = components?.day {
            _date = "\(month < 10 ? "0":"")\(month)-\(day)"
        }
        
        return EntryRow(_in: _in, lunch: lunch, out: out, total: total, _date: _date)
    }
    
    //date formatter
    static func formatDate(incoming: String) -> DateComponents? {
        let dateFormatter = DateFormatter(),
            calendar = Calendar.current
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        guard let date_format = dateFormatter.date(from: incoming) else {
            return nil
        }
        return calendar.dateComponents([.day, .month, .weekday], from: date_format)
    }
    
}

struct EntryRow {
    var _in: String? = nil
    var lunch: String? = nil
    var out: String? = nil
    var total: String? = nil
    var _date: String? = nil
}
