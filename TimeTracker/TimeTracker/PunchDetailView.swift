import UIKit

class PunchDetailView: UIViewController {
    
    var log : TimeEntries! {
        didSet {
            navigationItem.title = log.get_date()
        }
    }

    @IBOutlet weak var lblClockIn: UILabel!
    @IBOutlet weak var lblClockOut: UILabel!
    @IBOutlet weak var lblClockHours: UILabel!
    @IBOutlet weak var lblClockLunch: UILabel!
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblClockIn.text = log.get_in()
        lblClockOut.text = log.get_out()
        lblClockHours.text = log.get_hours()
        lblClockLunch.text = log.get_lunch()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension UILabel {
    
    public func addBorder( width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
}
