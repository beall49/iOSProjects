

#if !RX_NO_MODULE
    import RxSwift
    import RxCocoa
#endif
import RealmSwift
import Alamofire
import RxAlamofire

class API {

    let host = "https://timecard.ryanbeall.com/api"
    let insert_punch = "/insert",
        lunch_punch = "/lunch",
        get_punches = "/"

    var realm = try! Realm()
    var logObject:[[String: AnyObject]]?

    //get data
    func getData(){
        let getURL = "\(host)\(get_punches)"

        _ = request(.get, getURL)
            .observeOn(MainScheduler.instance)
            .flatMap { $0
                    .validate(statusCode: 200 ..< 300)
                    .validate(contentType: ["application/json"])
                    .rx.json()
            }
            .subscribe(
                onNext: { (json)  in
                    if let arr = json as? [[String: AnyObject]]{
                        self.logObject = arr
                    }
                },
                onError: { error in
                    print("\n\n\n\n\n\(error)\n\n\n\n\n")
                },
                onCompleted: {
                    guard let object = self.logObject else {
                        return
                    }

                    TimeEntries.deleteAllOfMe()

                    try! self.realm.write({
                        for row in object  {
                            self.realm.create(TimeEntries.self, value: row)
                        }
                    })
                }
            )
    }

    func postEvent(lunch: String){
        _ = getRequest(isLunch: (lunch != ""), lunch: lunch)
            .flatMap { $0
                .validate(statusCode: 200 ..< 300)
                .validate(contentType: ["application/json"])
                .rx.responseJSON()
            }
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: {  (r, json) in
                    if (r.statusCode == 200) {
                        self.getData()
                    }
                }
            )
    }

    //get correct request
    func getRequest(isLunch: Bool, lunch: String) -> Observable<DataRequest> {
        if (isLunch) {
            return request(.post,
                           self.host + self.lunch_punch,
                           parameters:  ["lunch": lunch],
                           encoding: JSONEncoding() as ParameterEncoding,
                           headers: nil )
        }
        return request(.post,
				self.host +
				self.insert_punch)
    }

}
