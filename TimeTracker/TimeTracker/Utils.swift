//
//  Utils.swift
//  TimeTracker
//
//  Created by Beall, Ryan on 10/25/16.
//  Copyright © 2016 com.g3.enterprises. All rights reserved.
//
import UIKit


public enum UIButtonBorderSide {
    case Top, Bottom, Left, Right
}



public class Utils {
    //create button borders
    public let cellIdentifier = "EntryCell"
    func createButtonBorders(btn : UIButton, top: UIRectCorner, bottom: UIRectCorner) -> CAShapeLayer {
            let shape = CAShapeLayer()
            let maskPath = UIBezierPath(roundedRect: btn.bounds,
                                            byRoundingCorners: [(top), (bottom)],
                                            cornerRadii: CGSize(width: 7, height: 7))
            shape.path = maskPath.cgPath
            return shape
    }
    
    
    public func addBorder(side: UIButtonBorderSide, width: CGFloat, button: UIButton) -> CALayer {
        let border = CALayer()
        
        switch side {
            case .Top:
                border.frame = CGRect(x: 0, y: 0, width: button.frame.size.width, height: width)
            case .Bottom:
                border.frame = CGRect(x: 0, y: button.frame.size.height - width, width: button.frame.size.width, height: width)
            case .Left:
                border.frame = CGRect(x: 0, y: 0, width: width, height: button.frame.size.height)
            case .Right:
                border.frame = CGRect(x: button.frame.size.width - width, y: 0, width: width, height: button.frame.size.height)
        }
        
        border.backgroundColor = UIColor.white.cgColor
        return border
    }
    
}




