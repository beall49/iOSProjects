//
//  ViewController.swift
//  TimeTracker
//
//  Created by Beall, Ryan on 10/18/16.
//  Copyright © 2016 com.g3.enterprises. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift
import RxRealm


class MainController: UIViewController, UITableViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
 
        //assign variable to watch data
        let tableData = Observable
                .collection(from: realm
                .objects(TimeEntries.self)
                .sorted(byKeyPath: "clock_date", ascending: false)
            )

        // update table cells whenever the data changes 💩
        _ = tableData.bindTo(
                tablePunches.rx.items(
                    cellIdentifier: Utils().cellIdentifier, cellType: TableViewCell.self)) { (row, element, cell) in 
                        if !(element.isInvalidated) {                            
                            let components = TimeEntries.formatDate(incoming: element.get_date())
                            if let month = components?.month, let day = components?.day, let day_int = components?.weekday {
                                    let new_day = DateFormatter().weekdaySymbols[day_int-1]
                                    cell.lblTitle!.text = "\(new_day): \(month)-\(day)"
                            }
                            cell.lblSubTitle!.text = "\(element.get_in())-\(element.get_out()): \(element.get_hours())"
                        }
                    }

        //clock in btn 🕑
        _ = btnIn.rx.tap
                .subscribe(
                    onNext: { x in 
                        self.api.postEvent(lunch: "")
                    }
                )

        //clock out btn 🕑
        _ = btnOut.rx.tap
                .subscribe(
                    onNext: { x in 
                        self.api.postEvent(lunch: "")
                    }
                )

        //30 min lunch 🍲
        _ = btn30.rx.tap
                .subscribe(
                    onNext: { x in
                         self.api.postEvent(lunch: "0.5")
                    }
                )

        //60 min lunch 🍲
        _ = btn60.rx.tap
                .subscribe(
                    onNext: {  x in 
                        self.api.postEvent(lunch: "1.0")
                    }
                )

        tablePunches
            .rx
            .setDelegate(self)
            .addDisposableTo(DisposeBag())
        
        _ = tablePunches
                .rx
                .itemSelected
                    .subscribe(onNext: {indexPath in
                        self.viewEntry(row: indexPath.row)
                    })
    }
    
    func viewEntry(row: Int) {
        func getStyle(incoming: String) -> UIAlertActionStyle{
            return (incoming.containsEmoji() ? .destructive : .default)
        }
        
        let entry = TimeEntries().getAlertEntries(index: row)
        
        guard let _date = entry._date,
                let lunch = entry.lunch,
                let out = entry.out,
                let total = entry.total,
                let _in = entry._in
            else {
                return
        }
        
        // err action button
        let entryAlert = UIAlertController(title: _date, message: nil, preferredStyle: .alert)
        
        entryAlert.addAction(UIAlertAction(title: _in, style: .default))
        entryAlert.addAction(UIAlertAction(title: lunch, style: getStyle(incoming: lunch)))
        entryAlert.addAction(UIAlertAction(title: out, style: getStyle(incoming: out)))
        entryAlert.addAction(UIAlertAction(title: total, style: getStyle(incoming: total)))
        entryAlert.addAction(UIAlertAction(title: "Done", style: .cancel))
        
        
        present(entryAlert, animated: true, completion: nil)
        tablePunches.deselectRow(at: [0, row], animated: true)
    }

    // on refresh
    func handleRefresh(refreshControl: UIRefreshControl) {
        api.getData()
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: {  timer in
            self.startSpinning = false
        })
    }

    //vars
    var realm = try! Realm()
    let api = API()

    @IBOutlet weak var btn60: UIButton! {
        didSet {
            btn60.layer.mask = Utils().createButtonBorders(btn: btn60, top: UIRectCorner.topRight, bottom: UIRectCorner.bottomRight)
        }
    }
    @IBOutlet weak var btnIn: UIButton! {
        didSet {
            btnIn.layer.mask = Utils().createButtonBorders(btn: btnIn, top: UIRectCorner.topLeft, bottom: UIRectCorner.bottomLeft)
        }
    }
    @IBOutlet weak var btnOut: UIButton! {
        didSet {
            btnOut.layer.addSublayer(Utils().addBorder(side: UIButtonBorderSide.Left, width: 1, button: btnOut))
        }
    }
    @IBOutlet weak var btn30: UIButton! {
        didSet {
            btn30.layer.addSublayer(Utils().addBorder(side: UIButtonBorderSide.Left, width: 1, button: btn30))
            btn30.layer.addSublayer(Utils().addBorder(side: UIButtonBorderSide.Right, width: 2, button: btn30))
        }
    }
    @IBOutlet weak var tablePunches: UITableView! {
        didSet {
            tablePunches.addSubview(self.refreshControl)
            tablePunches.addBorder()
        }
    }

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(refreshControl:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()

    public var startSpinning: Bool = false {
        didSet(newValue) {
            if (newValue) {
                startSpinning = true
            } else {
                startSpinning = false
                refreshControl.endRefreshing()
            }
        }
    }
}
extension UITableView {
    
    public func addBorder() {
        let border = CALayer()
        let width = CGFloat(1.5)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
extension String {
    func containsEmoji() -> Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
                case 0x3030, 0x00AE, 0x00A9,// Special Characters
                0x1D000...0x1F77F,          // Emoticons
                0x2100...0x27BF,            // Misc symbols and Dingbats
                0xFE00...0xFE0F,            // Variation Selectors
                0x1F900...0x1F9FF:          // Supplemental Symbols and Pictographs
                return true
            default:
                continue
            }
        }
        return false
    }
}


