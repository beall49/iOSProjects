//
//  InterfaceController.swift
//  TTW Extension
//
//  Created by Beall, Ryan on 11/7/16.
//  Copyright © 2016 com.g3.enterprises. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    let api = API()
    @IBOutlet var btn60Lunch: WKInterfaceButton!
    @IBOutlet var btn30Lunch: WKInterfaceButton!
    @IBOutlet var btnClockIn: WKInterfaceButton!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func clickIn() {
        buttonEnabler(btn: btnClockIn, bool: false)
        api.postEvent(lunch: "")
        disableDance(btn: btnClockIn)
    }
    
    @IBAction func click30() {
        buttonEnabler(btn: btn30Lunch, bool: false)
        api.postEvent(lunch: "0.5")
        disableDance(btn: btn30Lunch)
    }
    
    @IBAction func click60() {
        buttonEnabler(btn: btn60Lunch, bool: false)
        api.postEvent(lunch: "1.0")
        disableDance(btn: btn60Lunch)
    }
    
    func buttonEnabler(btn: WKInterfaceButton, bool: Bool){
        btn.setEnabled(bool)
    }
    
    func disableDance(btn: WKInterfaceButton){
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { timer in
            self.buttonEnabler(btn: btn, bool: true)
        })
    }
}
